<%--
  Created by IntelliJ IDEA.
  User: itu
  Date: 18/01/2023
  Time: 09:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import = "java.lang.*" %>
<%@ page import = "java.util.*" %>
<%@ page import = "data.*" %>
<%@ page import = "struct.StructInsertion" %>
<%
    StructInsertion sidm = new StructInsertion();
%>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    <link href="../assets/css/page.css" rel="stylesheet" type="text/css">
    <title>Imprimerie</title>
</head>
<body>
    <div id="content">
        <div id="menu">
            <form action="" method="POST">
                <label>
                    <select name="employe" class="liste-der">
                        <option>Employe 1</option>
                        <option>Employe 2</option>
                        <option>Employe 3</option>
                    </select>
                </label>
                <label>
                    <select name="mois" class="liste-der">
                        <%   for(String s : sidm.mois){ %>
                        <option><% out.println(s); %></option>
                        <% } %>
                    </select>
                </label>
                <label>
                    <input type="number" placeholder="nombre d'heure">
                </label>

                <input type="submit" value="Inserer">
            </form>
        </div>
        <div class="quit-div">
            <a href="../index.jsp"><div class="quit-bout">quitter</div></a>
        </div>
    </div>
</body>
</html>
