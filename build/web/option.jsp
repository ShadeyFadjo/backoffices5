<%-- 
    Document   : option
    Created on : 20 janv. 2023, 09:21:03
    Author     : ASUS
--%>
<%@page  import="entity.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Option</title>
    </head>
    <body>
        <h1>Options</h1>
        <form action="./updateoption">
        <table>
            <th>Clé</th>
            <th>Valeur</th>
            <tr>
                <td>Commission</td>
               <td><input  name="commission" type="range" min="0" max="50" value="<%=Parametre.getCommission()%>"></td>
            </tr>
            <tr>
                <td>Delai</td>
                <td><input  name="delai" type="range" min="0" max="50" value="<%=Parametre.getDelai()%>"></td>
            </tr>
            <tr>
                <td>Limite</td>
                 <td><input name="limite"  type="range" min="0" max="50" value="<%=Parametre.getLimite()%>"></td>
            </tr>
            <tr><td colspan="2"><input   type="submit" value="Modifier"></td></tr>
        </table1></form>
    </body>
</html>
