<%@page import="model.Categorie"%>
<%@page import="model.Genre"%>
<%@page import="model.Statistique"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import = "java.lang.*" %>
<%@ page import = "java.util.*,util.Connexion,java.sql.Connection,java.text.NumberFormat" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    <link href="../assets/css/page.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/fontawesome5-overrides.min.css">
    <title>Enchere</title>
</head>
    <% 
        
        Genre genre=Statistique.getPourcentagegenre();
        Genre gagnant=Statistique.getPourcentagegagnant();
        ArrayList<Categorie> lc=Categorie.getall();
        
        
    %>
    <body>
<!--        <div id="content">
        <div id="menu">
             
        </div>
        </div>-->
                <hr/>
                    <div id="wrapper">

<nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0" style="background: rgb(88,38,152);">
            <div class="container-fluid d-flex flex-column p-0"><a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                    <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-laugh-wink"></i></div>
                    <div class="sidebar-brand-text mx-3"><span>Enchere</span></div>
                </a>
                <hr class="sidebar-divider my-0">
                <ul class="navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item">
                        <a class="nav-link active" href="index.html">
                            <i class="fas fa-tachometer-alt"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="table.html">
                            <i class="fas fa-table"></i>
                            <span>Liste des encheres</span>
                        </a>
                    </li>
                      <li class="nav-item">
                        <a class="nav-link" href="table.html">
                            <i class="fas fa-table"></i>
                            <span>Liste des inscrits</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="option.jsp">
                            <i class="fas fa-key"></i>
                            <span>Modifier les options </span>
                        </a>
                    </li>
                   </ul>
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
                <div class="row"> 
                        <div class="col-lg-6 mb-4">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h5 class="text-primary fw-bold m-0">Tranche d'âge des inscrits sur le plateforme</h5>
                                </div>
                                <div class="card-body">
                                    <h4 class="small fw-bold">0-20<span class="float-end"><% out.println( NumberFormat.getInstance().format(Statistique.getpourcentagetranche(0, 20))); %>%</span></h4>
                                    <div class="progress mb-4">
                                        <div class="progress-bar bg-danger" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: <% out.print((int)Statistique.getpourcentagetranche(0, 20)); %>%;"><span class="visually-hidden">20%</span></div>
                                    </div>
                                    <h4 class="small fw-bold">21-40<span class="float-end"><% out.println( NumberFormat.getInstance().format(Statistique.getpourcentagetranche(21, 40))); %>%</span></h4>
                                    <div class="progress mb-4">
                                        <div class="progress-bar bg-warning" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <% out.print(Statistique.getpourcentagetranche(21, 40)); %>%;"><span class="visually-hidden">40%</span></div>
                                    </div>
                                    <h4 class="small fw-bold">41-60<span class="float-end"><% out.println( NumberFormat.getInstance().format(Statistique.getpourcentagetranche(41, 60))); %>%</span></h4>
                                    <div class="progress mb-4">
                                        <div class="progress-bar bg-primary" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <% out.print(Statistique.getpourcentagetranche(41, 60)); %>%;"><span class="visually-hidden">60%</span></div>
                                    </div>
                                    <h4 class="small fw-bold">60 et plus<span class="float-end"><% out.println( NumberFormat.getInstance().format(Statistique.getpourcentagetranche(60, 100))); %>%</span></h4>
                                    <div class="progress mb-4">
                                        <div class="progress-bar bg-info" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <% out.print(Statistique.getpourcentagetranche(60, 100)); %>%;"><span class="visually-hidden">80%</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>       
 
                                    <div class="col-lg-5 col-xl-4">
                            <div class="card shadow mb-4">
                                <div class="card-header d-flex justify-content-between align-items-center">
                                    <h6 class="text-primary fw-bold m-0">Répartition des inscrits sur le plateforme</h6>
                                    <div class="dropdown no-arrow"><button class="btn btn-link btn-sm dropdown-toggle" aria-expanded="false" data-bs-toggle="dropdown" type="button"><i class="fas fa-ellipsis-v text-gray-400"></i></button>
                                        <div class="dropdown-menu shadow dropdown-menu-end animated--fade-in">
                                            <p class="text-center dropdown-header">dropdown header:</p><a class="dropdown-item" href="#">&nbsp;Action</a><a class="dropdown-item" href="#">&nbsp;Another action</a>
                                            <div class="dropdown-divider"></div><a class="dropdown-item" href="#">&nbsp;Something else here</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="chart-area">
                                        <canvas data-bss-chart="
                                            {
                                                &quot;type&quot;:&quot;doughnut&quot;,
                                                &quot;data&quot;:{
                                                    &quot;labels&quot;:[
                                                        &quot;Homme&quot;,
                                                        &quot;Femme&quot;,
                                                        &quot;Referral&quot;
                                                    ],
                                                    &quot;datasets&quot;:[
                                                        {
                                                            &quot;label&quot;:&quot;&quot;,
                                                            &quot;backgroundColor&quot;:[
                                                                &quot;#4e73df&quot;,
                                                                &quot;#1cc88a&quot;,
                                                                &quot;#36b9cc&quot;
                                                            ],
                                                            &quot;borderColor&quot;:[
                                                                &quot;#ffffff&quot;,
                                                                &quot;#ffffff&quot;,
                                                                &quot;#ffffff&quot;
                                                            ],
                                                            &quot;data&quot;:[
                                                                
                                                            &quot;<% out.print(genre.getMasculin()+"   "); %>   &quot;,
                                                                &quot;<% out.print(genre.getFeminin()); %>&quot;
                                                            ]
                                                        }
                                                    ]
                                                },
                                                &quot;options&quot;:{
                                                    &quot;maintainAspectRatio&quot;:false,
                                                    &quot;legend&quot;:{
                                                        &quot;display&quot;:false,
                                                        &quot;labels&quot;:{
                                                            &quot;fontStyle&quot;:&quot;normal&quot;
                                                        }
                                                    },
                                                    &quot;title&quot;:{
                                                        &quot;fontStyle&quot;:&quot;normal&quot;
                                                    }
                                                }
                                            }">    
                                        </canvas>
                                    </div>
                                    <div class="text-center small mt-4">
                                        <span class="me-2">
                                            <i class="fas fa-circle text-primary"></i>
                                            &nbsp;Homme:<% out.print( NumberFormat.getInstance().format(genre.getMasculin())); %>
                                        </span>
                                        <span class="me-2">
                                            <i class="fas fa-circle text-success"></i>
                                            &nbsp;Femme:<% out.print( NumberFormat.getInstance().format(genre.getFeminin())); %>
                                        </span>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                
                        <div class="col-lg-6 mb-4">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h5 class="text-primary fw-bold m-0">Repartition des categories de produits mis en enchères: </h5>
                                </div>
                                <div class="card-body">
                                    
                                    
                                                    <% for(int i=0;i<lc.size();i++) {%>

                                    <h4 class="small fw-bold"><% out.println(lc.get(i).getCategorie()); %><span class="float-end"><% out.println( NumberFormat.getInstance().format(Statistique.getnbreencherebycategorie(lc.get(i).getIdCategorie()))); %>%</span></h4>
                                    <div class="progress mb-4">
                                        <div class="progress-bar bg-warning" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <% out.print(Statistique.getnbreencherebycategorie(lc.get(i).getIdCategorie())); %>%;"><span class="visually-hidden">40%</span></div>
                                    </div>
                                                    <% } %>  

                                    
                                </div>
                            </div>
                        </div>   
                                                    
                                                    
                                                    
                           <div class="col-lg-5 col-xl-4">
                            <div class="card shadow mb-4">
                                <div class="card-header d-flex justify-content-between align-items-center">
                                    <h6 class="text-primary fw-bold m-0">Répartition des gagnants des enchères</h6>
                                    <div class="dropdown no-arrow"><button class="btn btn-link btn-sm dropdown-toggle" aria-expanded="false" data-bs-toggle="dropdown" type="button"><i class="fas fa-ellipsis-v text-gray-400"></i></button>
                                        <div class="dropdown-menu shadow dropdown-menu-end animated--fade-in">
                                            <p class="text-center dropdown-header">dropdown header:</p><a class="dropdown-item" href="#">&nbsp;Action</a><a class="dropdown-item" href="#">&nbsp;Another action</a>
                                            <div class="dropdown-divider"></div><a class="dropdown-item" href="#">&nbsp;Something else here</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="chart-area">
                                        <canvas data-bss-chart="
                                            {
                                                &quot;type&quot;:&quot;doughnut&quot;,
                                                &quot;data&quot;:{
                                                    &quot;labels&quot;:[
                                                        &quot;Homme&quot;,
                                                        &quot;Femme&quot;,
                                                        &quot;Referral&quot;
                                                    ],
                                                    &quot;datasets&quot;:[
                                                        {
                                                            &quot;label&quot;:&quot;&quot;,
                                                            &quot;backgroundColor&quot;:[
                                                                &quot;#4e73df&quot;,
                                                                &quot;#1cc88a&quot;,
                                                                &quot;#36b9cc&quot;
                                                            ],
                                                            &quot;borderColor&quot;:[
                                                                &quot;#ffffff&quot;,
                                                                &quot;#ffffff&quot;,
                                                                &quot;#ffffff&quot;
                                                            ],
                                                            &quot;data&quot;:[
                                                                
                                                            &quot;<% out.print(gagnant.getMasculin()+"   "); %>   &quot;,
                                                                &quot;<% out.print(gagnant.getFeminin()); %>&quot;
                                                            ]
                                                        }
                                                    ]
                                                },
                                                &quot;options&quot;:{
                                                    &quot;maintainAspectRatio&quot;:false,
                                                    &quot;legend&quot;:{
                                                        &quot;display&quot;:false,
                                                        &quot;labels&quot;:{
                                                            &quot;fontStyle&quot;:&quot;normal&quot;
                                                        }
                                                    },
                                                    &quot;title&quot;:{
                                                        &quot;fontStyle&quot;:&quot;normal&quot;
                                                    }
                                                }
                                            }">    
                                        </canvas>
                                    </div>
                                    <div class="text-center small mt-4">
                                        <span class="me-2">
                                            <i class="fas fa-circle text-primary"></i>
                                            &nbsp;Homme:<% out.print( NumberFormat.getInstance().format(gagnant.getMasculin())); %>
                                        </span>
                                        <span class="me-2">
                                            <i class="fas fa-circle text-success"></i>
                                            &nbsp;Femme:<% out.print( NumberFormat.getInstance().format(gagnant.getFeminin())); %>
                                        </span>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>                         
                              </div>                       
                                 </div>                   
            <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/chart.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <script src="assets/js/theme.js"></script>                
    </body>
</html>
