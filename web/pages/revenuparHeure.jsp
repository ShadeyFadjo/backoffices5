<%--
  Created by IntelliJ IDEA.
  User: itu
  Date: 19/01/2023
  Time: 14:58
  To change this template use File | Settings | File Templates.
--%>
<%@page import="table.DetailMateriel"%>
<%@page import="table.DetailSalaire"%>
<%@page import="java.util.Vector"%>
<%@page import="connect.ConnectionDB"%>
<%@page import="java.sql.Connection"%>
<%@page import="table.Service"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    <link href="../assets/css/page.css" rel="stylesheet" type="text/css">
    <title>Imprimerie</title>
</head>
<body>
    <div id="content">
        <div id="menu">
            <br>
            <h2><% 
                                        Connection c=ConnectionDB.makaConPsql();
                int idservice=(int)Integer.parseInt(request.getParameter("service"));
                Service service=Service.getServiceById(idservice, c);
                out.print(service.getNomService()); %>: (marge <% out.println(service.getMarge()+"%"); %>)</h2>

            <h3>Depense salariale</h3>
            <div class="depense">

                <div class="div-title-depense"></div>
                <div class="div-title-depense">Salaire</div>
                <div class="div-title-depense">Nombre d'heure</div>
                <div class="div-title-depense">Total</div>
                <% 
                        Vector<DetailSalaire> ls=DetailSalaire.getDetailSalaireByIdService(idservice,c);
                        for(int i=0;i<ls.size();i++)
                        {
                %>

                <div> <% out.println(ls.get(i).getPoste().getNomPoste()); %></div>
                <div><% out.println(ls.get(i).getPoste().getSalParHeure()); %></div>
                <div><% out.println(ls.get(i).getNbrHeure()); %></div>
                <div><% out.println(ls.get(i).getPoste().getSalParHeure()*ls.get(i).getNbrHeure()); %></div>
                
                <% } %>

            </div>

            <h3>Depense materielle</h3>
            <div class="depense">
                <div class="div-title-depense"></div>
                <div class="div-title-depense">Qte utilise</div>
                <div class="div-title-depense">Prix unitaire</div>
                <div class="div-title-depense">Total</div>
<% 
                        Vector<DetailMateriel> lm=DetailMateriel.getDetailMaterielByIdService(idservice,c);
                        //for(int i=0;i<lm.size();i++)
                        //{
                %>
                <div><%  %></div>
                <div>3</div>
                <div>2000</div>
                <div>6000</div>
                                <% //} %>

            </div>

            <div><h3>Prix du service: <% out.println(DetailSalaire.getTotal(idservice, c)); %></h3></div>
            <br>
        </div>
        <br>
        <div class="quit-div">
            <a href="./choixServices.jsp"><div class="quit-bout">retour</div></a>
        </div>
    </div>
</body>
</html>
