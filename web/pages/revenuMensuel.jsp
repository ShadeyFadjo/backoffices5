<%--
  Created by IntelliJ IDEA.
  User: itu
  Date: 18/01/2023
  Time: 11:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import = "java.lang.*" %>
<%@ page import = "java.util.*" %>
<%@ page import = "data.*" %>
<%@ page import = "struct.StructInsertion" %>
<%
    StructInsertion sidm = new StructInsertion();
%>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    <link href="../assets/css/page.css" rel="stylesheet" type="text/css">
    <title>Imprimerie</title>
</head>
<body>
    <div id="content">
        <div class="menu2">
            <select name="mois" class="liste-der">
                <%   for(String s : sidm.mois){ %>
                <option><% out.println(s); %></option>
                <% } %>
            </select>
            <div id="depense-benefice">
                <div class="colonne">Depense salariale</div>
                <div class="colonne">Depense materielle</div>
                <div class="colonne">Benefice</div>
            </div>
        </div>
    </div>
</body>
</html>
