<%-- 
    Document   : allDemande
    Created on : 20 janv. 2023, 11:12:05
    Author     : ASUS
--%>
<%@page import="entity.MouvementCompte,java.util.ArrayList" %>

<%ArrayList<MouvementCompte>mc=(ArrayList<MouvementCompte>)request.getAttribute("liste");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Demandes</h1>
        <table border="1">
            <th>idUtilisateur</th>
            <th>somme</th>
            <th>Date de demande</th>
            <% for (int i = 0; i < mc.size(); i++) {%>
            <tr>
                <td><%=mc.get(i).getIdUtilisateur()%></td>
                <td><%=mc.get(i).getSomme()%></td>
                <td><%=mc.get(i).getDemande()%></td>
                <td><a href="/accept?id="<%=mc.get(i).getDemande()%>>Accepter </a></td>
                <td><a href="url">Refuser</a></td>
            </tr>
            <% }%>
        </table>
    </body>
</html>
