/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.Connexion;

/**
 *
 * @author ASUS
 */
public class Parametre {
    private static Integer delai;
    private static Double commission;
    private static Integer limite;

    public static int getDelai() {
        if(delai==null)try {
            initialize(null);
        } catch (Exception ex) {
            Logger.getLogger(Parametre.class.getName()).log(Level.SEVERE, null, ex);
        }
        return delai;
    }

    public static void setDelai(int delai) {
        Parametre.delai = delai;
    }

    public static double getCommission() {
        if(commission==null)try {
            initialize(null);
        } catch (Exception ex) {
            Logger.getLogger(Parametre.class.getName()).log(Level.SEVERE, null, ex);
        }
        return commission;
    }

    public static void setCommission(double commission) {
        Parametre.commission = commission;
    }

    public static int getLimite() {
         if(limite==null)try {
            initialize(null);
        } catch (Exception ex) {
            Logger.getLogger(Parametre.class.getName()).log(Level.SEVERE, null, ex);
        }
        return limite;
    }

    public static void setLimite(int limite) {
        Parametre.limite = limite;
    }
   
    public static void initialize(Connection con) throws Exception
    {
        try {
            int tester=0;
            if(con==null){tester=1;con=Connexion.createConnection(true);}
            try (Statement stmt = con.createStatement()) {
                String sql="select * from parametre where cle='delai'";
                ResultSet rs=stmt.executeQuery(sql);
                while(rs.next()){setDelai(rs.getInt("valueNumber"));}
                sql="select * from parametre where cle='commission'";
                rs=stmt.executeQuery(sql);
                while(rs.next()){setCommission(rs.getDouble("valueNumber"));}
                sql="select * from parametre where cle='limite'";
                rs=stmt.executeQuery(sql);
                while(rs.next()){setLimite(rs.getInt("valueNumber"));}
                rs.close();
            }
            if(tester==1){con.close();}
        } catch (Exception e) {
            throw e;
        }
    }
    
    public static void update(Connection con) throws Exception
    {
        try {
            Statement stmt=con.createStatement();
            String sql="update parametre set valueNumber="+getDelai()+" where cle='delai'";
            stmt.executeUpdate(sql);
             sql="update parametre set valueNumber="+getLimite()+" where cle='limite'";
            stmt.executeUpdate(sql);
             sql="update parametre set valueNumber="+getCommission()+" where cle='commission'";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (Exception e) {
            throw e;
        }
    }
    public static String tostring() {
        return "Parametre{limite:" +limite+"  commission"+commission+" '}'";
    }

}
