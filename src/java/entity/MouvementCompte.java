/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class MouvementCompte {
    int idMouvementCompte;
    int idUtilisateur;
    int idEtat;
    Date mouvement;
    double somme;
    Date demande;

    public MouvementCompte() {
    }

    public MouvementCompte(int idMouvementCompte, int idUtilisateur, int idEtat, Date mouvement, double somme, Date demande) {
        this.idMouvementCompte = idMouvementCompte;
        this.idUtilisateur = idUtilisateur;
        this.idEtat = idEtat;
        this.mouvement = mouvement;
        this.somme = somme;
        this.demande = demande;
    }
    
    
    
    public int getIdMouvementCompte() {
        return idMouvementCompte;
    }

    public void setIdMouvementCompte(int idMouvementCompte) {
        this.idMouvementCompte = idMouvementCompte;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public int getIdEtat() {
        return idEtat;
    }

    public void setIdEtat(int idEtat) {
        this.idEtat = idEtat;
    }

    public Date getMouvement() {
        return mouvement;
    }

    public void setMouvement(Date mouvement) {
        this.mouvement = mouvement;
    }

    public double getSomme() {
        return somme;
    }

    public void setSomme(double somme) {
        this.somme = somme;
    }

    public Date getDemande() {
        return demande;
    }

    public void setDemande(Date demande) {
        this.demande = demande;
    }
    
    public static ArrayList<MouvementCompte> getAll(Connection con) throws Exception
    {
        String sql="select * from MouvementCompte";
        try {
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery(sql);
            ArrayList<MouvementCompte> liste=new ArrayList<MouvementCompte>();
            while(rs.next())
            {
                liste.add(new MouvementCompte(rs.getInt("idmouvementcompte"),rs.getInt("idutilisateur"),
                        rs.getInt("idetat"),new Date(rs.getString("datemouvement")),rs.getDouble("somme"),null));
            }
            return liste;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }   
    }
    
     public static ArrayList<MouvementCompte> getDemande(Connection con) throws Exception
    {
        String sql="select * from MouvementCompte where idEtat=1";
        try {
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery(sql);
            ArrayList<MouvementCompte> liste=new ArrayList<>();
            while(rs.next())
            {
               String format=rs.getString("datedemande").replace("-", "/").replace(".", "-").split("-")[0];
                       
                liste.add(new MouvementCompte(rs.getInt("idmouvementcompte"),rs.getInt("idutilisateur"),
                        rs.getInt("idetat"),null,rs.getDouble("somme"),new Date(format)));
            }
            rs.close();
            stmt.close();
            return liste;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }   
    }
     public void accepter(Connection con) throws Exception
     {
         try {
             Statement stmt=con.createStatement();
             String sql="update mouvementcompte set etat=2 datemouvement=now() where idMouvementCompte="+getIdMouvementCompte();
             stmt.executeUpdate(sql);
         } catch (Exception e) {
             e.printStackTrace();
             throw e;
         }
     }
     
     public void refuser(Connection con) throws Exception
     {
         try {
             Statement stmt=con.createStatement();
             String sql="update mouvementcompte set etat=3 datemouvement=now() where idMouvementCompte="+getIdMouvementCompte();
             stmt.executeUpdate(sql);
         } catch (Exception e) {
             e.printStackTrace();
             throw e;
         }
     }
     
     
}
