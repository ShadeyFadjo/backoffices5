/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class Categorie {
    int idCategorie;
    String categorie;

    public Categorie(int idCategorie, String categorie) {
        this.idCategorie = idCategorie;
        this.categorie = categorie;
    }

    public Categorie() {
    }
    
    public int getIdCategorie() {
        return idCategorie;
    }

    public void setIdCategorie(int idCategorie) {
        this.idCategorie = idCategorie;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }
    public Categorie[] getAll(Connection con) throws Exception
    {
        try {
            String sql="select * from categorie";
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery(sql);
            ArrayList<Categorie> all=new ArrayList();
            while (rs.next()) {
                all.add(new Categorie(rs.getInt("idCategorie"),rs.getString("categorie")));
            }
            rs.close();
            stmt.close();
            return (Categorie[]) all.toArray();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
    
    public void insert(Connection con) throws Exception{
        try {
            Statement stmt=con.createStatement();
            stmt.executeUpdate("insert into categorie(categorie) values('"+this.getCategorie()+"')");
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void update(Connection con) throws Exception
    {
        try {
            Statement stmt=con.createStatement();
            stmt.executeUpdate("update categorie set categorie='" +this.getCategorie()+"' where idCategorie="+this.getIdCategorie());
            stmt.close();
        } catch (Exception e) {
            throw e;
                    }
    }
}
