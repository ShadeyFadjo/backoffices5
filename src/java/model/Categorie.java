/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dao.Dbconnect;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Categorie {
    private int idCategorie;
    private String categorie;

    public int getIdCategorie() {
        return idCategorie;
    }

    public void setIdCategorie(int idCategorie) {
        this.idCategorie = idCategorie;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }
    
    public static ArrayList<Categorie> getall()throws Exception
    {
        ArrayList<Categorie> lc=new ArrayList<Categorie>();
        Dbconnect d=new Dbconnect();
        Connection c=d.getConnect();
        Statement stmt=null;
        ResultSet rs=null;
        String sql="select * from categorie;";
        try {
            stmt=c.createStatement();
            rs=stmt.executeQuery(sql);
            while(rs.next())
            {
                Categorie cat = new Categorie();
                cat.setIdCategorie(rs.getInt("idcategorie"));
                cat.setCategorie(rs.getString("categorie"));
                lc.add(cat);
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return lc;
    }
}
