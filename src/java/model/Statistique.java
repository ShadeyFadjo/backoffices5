
package model;

import dao.Dbconnect;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Statistique {
    //repartition inscrit
    //             gagnants
    // gagnant par tranche d'age
    //repartition categorie des produits en enchere
    
    public static Genre getPourcentagegenre() throws Exception
    {
        Dbconnect d=new Dbconnect();
        Connection c=d.getConnect();
        Genre g=new Genre();
        Statement stmt=null;
        ResultSet rs=null;
        String sql="select * from pourcentagegenre;";
        try {
            stmt=c.createStatement();
            rs=stmt.executeQuery(sql);
            while(rs.next())
            {
                g.setMasculin(rs.getDouble("masculin"));
                g.setFeminin(rs.getDouble("feminin"));
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
        return g;
    }
    public static Genre getPourcentagegagnant() throws Exception
    {
        Dbconnect d=new Dbconnect();
        Connection c=d.getConnect();
        Genre g=new Genre();
        Statement stmt=null;
        ResultSet rs=null;
        String sql="select * from pourcentagegagnant;";
        try {
            stmt=c.createStatement();
            rs=stmt.executeQuery(sql);
            while(rs.next())
            {
                g.setMasculin(rs.getDouble("masculin"));
                g.setFeminin(rs.getDouble("feminin"));
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
        return g;
    }
    
    public static double getnbreutilisateur() throws Exception
    {
        double ret=0;
        Dbconnect d=new Dbconnect();
        Connection c=d.getConnect();
        Statement stmt=null;
        ResultSet rs=null;
        String sql="select count(*) from age ;";
        
        try {
            stmt=c.createStatement();
            rs=stmt.executeQuery(sql);
            while(rs.next())
            {
                ret=rs.getDouble("count");
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
        return ret;
    }
    
    public static double getpourcentagetranche(int a,int b) throws Exception
    {
        double ret=0;
        Dbconnect d=new Dbconnect();
        Connection c=d.getConnect();
        Statement stmt=null;
        ResultSet rs=null;
        String sql="select count(*) from age where age between "+a+"and "+b+";";
        
        try {
            stmt=c.createStatement();
            rs=stmt.executeQuery(sql);
            while(rs.next())
            {
                ret=rs.getDouble("count");
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
        return (ret*100)/getnbreutilisateur();
    }
    
    public static double getnbresdencheres() throws Exception
    {
        double ret=0;
        Dbconnect d=new Dbconnect();
        Connection c=d.getConnect();
        Statement stmt=null;
        ResultSet rs=null;
        String sql="select count(*) from enchere ;";
        
        try {
            stmt=c.createStatement();
            rs=stmt.executeQuery(sql);
            while(rs.next())
            {
                ret=rs.getDouble("count");
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
        return ret;
    }
    
    public static double getnbreencherebycategorie(int idcategorie) throws Exception
    {
        double ret=0;
        Dbconnect d=new Dbconnect();
        Connection c=d.getConnect();
        Statement stmt=null;
        ResultSet rs=null;
        String sql="select count(*) from enchere where idcategorie="+idcategorie+";";
        
        try {
            stmt=c.createStatement();
            rs=stmt.executeQuery(sql);
            while(rs.next())
            {
                ret=rs.getDouble("count");
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
        return (ret*100)/getnbresdencheres();
    }

}
